# Copyright 2012 Sam Lade
#
# This file is part of Synchronal Server.
#
# Synchronal Server is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Synchronal Server is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Synchronal Server.  If not, see <http://www.gnu.org/licenses/>.
from django.db import models
from django.contrib.auth.models import User

class SynchronalRead(models.Model):
    user = models.ForeignKey(User)
    stream_name = models.CharField(max_length=100)
    read_id = models.CharField(max_length=20)
    def __unicode__(self):
        return self.stream_name

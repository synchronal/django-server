# Copyright 2012 Sam Lade
#
# This file is part of Synchronal Server.
#
# Synchronal Server is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Synchronal Server is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Synchronal Server.  If not, see <http://www.gnu.org/licenses/>.
import json
import re
from inspect import getsource

from django.http import HttpResponse
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from django.db import IntegrityError
from django.core.exceptions import ObjectDoesNotExist
from django.views.decorators.csrf import csrf_exempt

import synchronal
from synchronal.models import SynchronalRead

# no point using the CSRF protection system for an API
@csrf_exempt
def home(request):
    try:
        return _home(request)
    except Exception as e:
        return gen_json(3, "error", "Server error: {}".format(e.__repr__()))

def _home(request):
    post = request.REQUEST
    user = request.user
    if "action" not in post:
        return gen_json(1, "error", "An action must be specified.")

    action = post["action"]
    if action not in ["source", "register", "login", "get", "set"]:
        return gen_json(1, "error", "Unknown action")

    if not request.POST:
        if action not in ["source", "get"]:
            return gen_json(1, "error", "Requests changing values must be POST")

    if action == "source":
        res = []
        for module in (synchronal.models, synchronal.views, synchronal.urls):
            res.append("-----\n{}\n-----\n\n{}".format(module.__name__, getsource(module)))
        return HttpResponse("\n\n\n".join(res), mimetype="text/plain")

    if not user.is_authenticated():
        if action not in ["login", "register"]:
            return gen_json(2, "error", "You must be logged in to do that.")

        if not ("user" in post) & ("pass" in post):
            return gen_json(1, "error", "You must supply a username and password.")
        username = post["user"]
        password = post["pass"]
        if (not username) | (not password) | (len(username) > 30) | (re.search(r"[^\w@+.\-]", username) is not None):
            return gen_json(11, "error", "Invalid username or password.")

        if action == "register":
            try:
                User.objects.create_user(username, "", password)
            except IntegrityError:
                return gen_json(12, "error", "Username taken.")
            return gen_json(0)

        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            return gen_json(0)
        return gen_json(13, "error", "Invalid login credentials.")

    if action not in ["get", "set"]:
        return gen_json(4, "error", "You are already logged in.")

    if "stream" not in post:
        return gen_json(1, "error", "You must specify a stream name.")
    stream_name = post["stream"]
    if (not stream_name) | (len(stream_name) > 100):
        return gen_json(22, "error", "Invalid stream name")

    try:
        stream = user.synchronalread_set.get(stream_name=stream_name)
    except ObjectDoesNotExist:
        stream = None

    if action == "get":
        if stream is None:
            return gen_json(0, "tid", "0")
        return gen_json(0, "tid", stream.read_id)

    if "tid" not in post:
        return gen_json(1, "error", "You must specify a tweet ID.")
    tid = post["tid"]
    try:
        tidi = int(tid)
    except ValueError:
        tidi = -1
    if (tidi < 0) | (tidi > 2**64):
        return gen_json(23, "error", "Invalid tweet ID.")

    if stream is None:
        stream = SynchronalRead(stream_name=stream_name, read_id=tid, user=user)
    else:
        stream.read_id = tid
    stream.save()
    return gen_json(0)

def gen_json(code, dataname=None, dataval=None):
    res = {"code":code}
    if dataname is not None:
        res[dataname] = dataval
    return HttpResponse(json.dumps(res), mimetype="application/json")

# return codes
# 0 success
# 1 request error
# 2 authentication error
# 3 server error
# 4 already authenticated
# 11 invalid username or password
# 12 username taken
# 13 invalid login
# 22 invalid stream name
# 23 invalid tweet ID
